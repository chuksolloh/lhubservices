<?php

namespace classes;

/**
 * Description of PinGenerator
 *
 * @author Chuks
 */

class PinGenerator {
	//put your code here
	const MAX_CHARS = 8;
	
	private static $filename;
	private static $max = 20;
	private static $str = '';

	public static function generatePins($max = 0) {
		$max > 0 ? self::$max = $max : self::$max;
		self::createFile();
		self::closeFile(); // Close file
		self::deleteFile(); // Delete file
	}
	
	protected static function createFile() {
		self::$filename = 'pins.txt'; // Open file
		
		try {
			fopen(self::$filename, 'w'); // Open file or create if it doesn't exists
			
			if (file_exists(self::$filename)) {
				file_put_contents(self::$filename, self::buildRandomChars(), LOCK_EX) !== false ? self::downloadFile() : '';
			}
		} catch (\Exception $ex) {
			print_r($ex->getTraceAsString());
		}
	}
	
	protected static function closeFile() { 
		fclose(fopen(self::$filename, 'r')) or die('Failed to close file - '. self::$filename);
	}
	
	protected static function buildRandomChars() { 
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; // Valid characters
		$charlen = strlen($chars);
		$randstr = '';
		
		for ($i = 0; $i < self::$max; $i++) {
			for ($y = 0; $y < self::MAX_CHARS; $y++) {
				$randstr .= $chars[rand(0, $charlen - 1)];
			}
			self::$str .= $randstr .', ';
			$randstr = '';
		}
		return trim(self::$str, ', ');
	}
	
	protected static function downloadFile() {
		if (file_exists(self::$filename)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'. basename(self::$filename) .'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: Public');
			header('Content-Length: '. filesize(self::$filename));
			readfile(self::$filename);
			exit;
		}
	}
	
	protected static function deleteFile() { 
		unlink(basename(self::$filename)) or die('Failed to delete file '. self::$filename);
	}
}
