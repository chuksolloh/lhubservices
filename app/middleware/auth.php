<?php

namespace middleware;

/**
 * Description of auth
 *
 * @author chuksolloh
 */
class Auth extends \Slim\Middleware {
	//put your code here
	private $ipaddress = array('127.0.0.1');
	private $headers;
	
	public function __construct($headers) {
		$this->headers = $headers;
	}
	
	public function call() {
		// Check if Ipaddress is local.
		if (!in_array($this->app->request()->getIp(), $this->ipaddress)) {
			// Unauthorized ipaddress access.
			$this->app->halt(403, 'Unauthorized ip-address access');
		} 
		$this->next->call();
	}
}
