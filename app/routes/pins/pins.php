<?php

use classes\PinGenerator;

// Middleware - Auth
$app->add(new \middleware\Auth(apache_request_headers())); 

$app->group('/api/v1', function() use($app){
	$app->get('/pins', function() use($app){
		$app->response->body(PinGenerator::generatePins());
	});
});
